import * as angular from 'angular';
import { ICompileProvider, ILogProvider } from 'angular';
import { AudioPlayerContainerComponent } from './AudioPlayerContainer/audio-player-container.component';
import Directives from './directives';
import Filters from './filters';

export default angular
  .module('player', [
    Filters,
    Directives
  ])
  .config(/*ngInject*/ ($compileProvider: ICompileProvider, $logProvider: ILogProvider) => {
    // Configure compile service
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.onChangesTtl(3);
    $compileProvider.strictComponentBindingsEnabled(true);

    // Configure log service
    $logProvider.debugEnabled(true);
  })
  .component('audioPlayerContainer', AudioPlayerContainerComponent)
  .name;
