import { AudioSource } from './AudioSource';

export class Track {
  public id: number = 0;
  public src: string;
  public lastVolume: number;
  public audio: AudioSource;

  constructor(src: string, audio: AudioSource) {
    this.src = src;
    this.audio = audio;
  }

  get paused(): boolean {
    return this.audio.paused;
  }

  get volume(): number {
    return this.audio.volume;
  }

  set volume(value: number) {
    this.audio.volume = value < 0 ? 0 : (value > 1 ? 1 : value);
  }

  get duration(): number {
    return this.audio.duration;
  }

  get time(): number {
    return this.audio.currentTime;
  }

  set time(time: number) {
    this.audio.currentTime = time;
  }

  public play(): any {
    return this.audio.play();
  }

  public pause(): any {
    return this.audio.pause();
  }

  public mute(): this {
    this.audio.muted = true;

    return this;
  }

  public unmute(): this {
    this.audio.muted = false;

    return this;
  }

  public load(): this {
    this.audio.load();

    return this;
  }
}
