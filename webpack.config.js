const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');

const ENV = process.env.NODE_ENV || 'development';

module.exports = {
  entry: {
    common: './src/common.js',
    app: './src/index.ts'
  },
  devtool: 'source-map',
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'web')
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          'ng-annotate-loader',
          {
            loader: 'awesome-typescript-loader',
            options: {
              sourceMap: true,
              forkChecker: true,
              useTranspileModule: true,
              silent: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'ng-annotate-loader',
          'babel-loader'
        ]
      },
      {
        test: /\.html/,
        use: [
          {
            loader: 'html-loader',
            options: {
              ignoreCustomFragments: [/\{\{.*?}}/],
              root: path.resolve(__dirname, 'app')
              // attrs: ['img:src', 'link:href']
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      }
    ]
  },
  devServer: {
    contentBase: './web'
  },
  resolve: {
    extensions: ['.ts', '.js', '.json', '.html'],
    alias: {
      app: path.resolve(__dirname, 'src/')
    }
  },
  plugins: [
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'index.ejs'),
      title: 'Development'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common' // Specify the common bundle's name.
    }),
    new webpack.DefinePlugin({
      'process.env': {
        ENV: JSON.stringify(ENV),
        NODE_ENV: JSON.stringify(ENV)
      },
      __PROD__: JSON.stringify(ENV === 'production'),
      __DEV__: JSON.stringify(ENV === 'development')
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    // new webpack.ProvidePlugin({
    //   __CONFIG__: 'Config'
    // })
  ]
};
