import { IComponentOptions } from 'angular';
import * as angular from 'angular';
import { IScope } from 'angular';
import { IAudioTrack } from '../IAudioTrack';

class AudioPlayerContainerController implements ng.IComponentController {
  public sources: IAudioTrack[];
  private $dragover: JQuery;

  constructor(private $element: JQuery, private $scope: IScope) {
    'ngInject';

    this.handleDrop = this.handleDrop.bind(this);
    this.handleDragOver = this.handleDragOver.bind(this);
    this.handleDragLeave = this.handleDragLeave.bind(this);

    this.sources = [];
  }

  public $postLink(): void {
    this.$dragover = angular.element(document.querySelector('.dragover'));

    window.addEventListener('drop', this.handleDrop);
    window.addEventListener('dragover', this.handleDragOver);
    window.addEventListener('dragleave', this.handleDragLeave);
  }

  public $onDestroy(): void {
    window.removeEventListener('drop', this.handleDrop);
    window.removeEventListener('dragover', this.handleDragOver);
    window.removeEventListener('dragleave', this.handleDragLeave);
  }

  public onRemove($event: { url: string }) {
    this.sources = this.sources.filter((t) => t.url !== $event.url);
  }

  private handleDragOver(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();

    if (!this.$dragover.hasClass('visible')) {
      this.$dragover.addClass('visible');
    }
  }

  private handleDragLeave(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();

    if (this.$dragover.hasClass('visible')) {
      this.$dragover.removeClass('visible');
    }
  }

  private handleDrop(e: DragEvent) {
    e.preventDefault();
    e.stopPropagation();

    this.$dragover.removeClass('visible');

    const files = (e as any).dataTransfer.files;
    const sources = [];

    if (files.length === 0) {
      const url = e.dataTransfer.getData('text');

      if (url.search(/https?:\/\//) === 0) {
        sources.push({
          url,
          title: 'New Track'
        });
      }
    } else {
      for (const file of files) {
        sources.push({
          url: URL.createObjectURL(file),
          title: file.name
        });
      }
    }

    this.sources = this.sources.concat(sources);

    this.$scope.$applyAsync();
  }
}

export const AudioPlayerContainerComponent: IComponentOptions = {
  template: require('./audio-player-container.html'),
  controller: AudioPlayerContainerController
};
