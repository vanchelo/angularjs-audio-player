import * as template from './audio-slider.html';

class AudioSliderController implements ng.IController {
  public time: number;
  public duration: number;
  public onClick: ($event: { $event: number }) => void;

  constructor() {
    //
  }

  public handleClick({ currentTarget, offsetX }: any) {
    let { width } = currentTarget.getBoundingClientRect();
    let time = 1 / (width / offsetX) * this.duration;

    this.onClick({ $event: 0 | time });

    if (__DEV__) {
      console.groupCollapsed('slider click', `${(0 | (time / 60))}m`);
      console.log('[x]', offsetX);
      console.log('[width]', width);
      console.log('[time]', time);
      console.log('[duration]', this.duration);
      console.log('[audio.currentTime]', this.time);
      console.groupEnd();
    }
  }
}

export const AudioSliderComponent: ng.IComponentOptions = {
  bindings: {
    time: '<',
    duration: '<',
    onClick: '&'
  },
  template,
  controller: AudioSliderController
};
