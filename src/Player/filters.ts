import * as angular from 'angular';
import { toTime } from "../utils";

export default angular
  .module('player.filter', [
    // Dependencies
  ])
  .filter('int', () => (value: number): number => 0 | value)
  .filter('time', () => (value: number): string => toTime(value))
  .name;
