export interface IAudioTrack {
  title: string
  url: string
}
