import { Track } from './Track';

export interface ApplyFunc {
  (track: Track, index: number): boolean | Track | void;
}
