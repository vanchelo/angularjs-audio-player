export function toTime(value: number): string {
  const secNum: number = 0 | value;
  const minutes: number = 0 | secNum / 60;
  const seconds: number = 0 | (secNum / 60 - minutes) * 60;

  return `${minutes}:${seconds < 10 ? 0 : ''}${seconds}`;
}
