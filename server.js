const http = require('http');
const fs = require('fs');
const path = require('path');
const mime = require('mime');
const server = http.createServer(createServer);

let port = process.env.PORT || 8080;
let host = process.env.HOST || 'localhost';

server.on('error', function (err) {
  console.log('Error:', err.message);

  server.listen(++port);
});

server.listen({ host, port }, () => {
  console.log(`Server running at http://${host}:${port}/`);
});

function createServer(req, res) {
  const socket = req.socket;

  res.on('finish', function () {
    socket.removeAllListeners('timeout');
    socket.setTimeout(5000, function () {
      socket.destroy();
    });
  });

  let filePath = `.${req.url}`;

  if (filePath === './') {
    filePath = './index.html';
  }

  const extname = String(path.extname(filePath)).toLowerCase();
  let contentType = 'text/html';

  contentType = mime.lookup(extname) || 'application/octect-stream';

  fs.readFile(filePath, function (error, content) {
    if (error) {
      if (error.code === 'ENOENT') {
        fs.readFile('./index.html', function (error, content) {
          res.writeHead(200, { 'Content-Type': 'text/html' });
          res.end(content, 'utf-8');
        });
      } else {
        res.writeHead(500);
        res.end(`Sorry, check with the site admin for error: ${error.code}.`);
        res.end();
      }
    } else {
      let stat = fs.statSync(filePath);

      const headers = {
        'Accept-Ranges': 'bytes',
        'Content-Type': contentType,
        'Content-Length': stat.size
      };

      if (contentType === 'audio/mpeg') {
        let range = 0;

        if (req.headers.range) {
          range = +req.headers.range.replace(/[^\d]/g, '');
        }

        headers['Pragma'] = 'no-cache';
        headers['Date'] = new Date().toGMTString();
        headers['Cache-Control'] = 'max-age=0';
        headers['Content-Disposition'] = 'attachment';
        headers['Content-Length'] = stat.size - range;
        headers['Content-Range'] = `bytes ${range}-${stat.size - 1}/${stat.size}`;
        res.writeHead(206, headers);

        const readStream = fs.createReadStream(filePath, {
          start: range,
          end: stat.size
        });

        // Stream chunks to response
        readStream.pipe(res);
      } else {
        res.writeHead(200, headers);
        res.end(content, 'utf-8');
      }
    }
  });
}

