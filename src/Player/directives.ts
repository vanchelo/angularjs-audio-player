import * as angular from 'angular';
import { AudioPlayerComponent } from './AudioPlayer/audio-player.component';
import { AudioSliderComponent } from './AudioSlider/audio-slider.component';
import { AudioTrack } from './AudioTrack/audio-track';

export default angular
  .module('player.directives', [])
  .component('audioPlayer', AudioPlayerComponent)
  .component('audioSlider', AudioSliderComponent)
  .directive('audioTrack', AudioTrack)
  .name;
