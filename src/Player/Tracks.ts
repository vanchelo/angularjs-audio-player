import { Track } from './Track';

export interface Tracks {
  [id: number]: Track;
}
