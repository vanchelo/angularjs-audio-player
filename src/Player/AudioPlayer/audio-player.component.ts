import { IComponentController, IComponentOptions } from 'angular';
import { Track } from '../Track';
import { Tracks } from '../Tracks';
import { ApplyFunc } from '../ApplyFunc';
import createSequence from '../create-sequence';
import { IAudioTrack } from '../IAudioTrack';

/**
 * @constructor
 * @property {Track} active
 * @property {Object<Track>} tracks
 */
export class AudioPlayer implements IComponentController {
  public totalTracks: number = 0;
  public muted = false;
  public active: Track;
  public playing = false;
  public volume = 100;
  public sources: IAudioTrack[];

  private _id: number = 0;
  private tracks: Tracks = {};
  private sequence = createSequence(0);

  constructor() {
    this.onKeydown = this.onKeydown.bind(this);
  }

  public onKeydown(e: KeyboardEvent): void {
    switch (e.key) {
      case ' ':
        this.playPause();
        break;

      case 'ArrowUp':
        this.prev();
        break;

      case 'ArrowDown':
        this.next();
        break;

      case 'ArrowLeft':
        this.backward();
        break;

      case 'ArrowRight':
        this.forward();
        break;
    }
  }

  public $postLink(): void {
    window.addEventListener('keyup', this.onKeydown, true);
  }

  public $onInit(): void {
    this.totalTracks = this.sources.length;
  }

  public $onChanges(changes: ng.IOnChangesObject): void {
    if (changes.sources && changes.sources.currentValue) {
      this.totalTracks = changes.sources.currentValue.length;
    }
  }

  public $onDestroy(): void {
    window.removeEventListener('keyup', this.onKeydown, true);
  }

  public next(): void {
    if (this.active && this.tracks[this.active.id + 1]) {
      this.tracks[this.active.id + 1].play();
    } else {
      this.firstTrack().play();
    }
  }

  public prev(): void {
    if (this.active && this.tracks[this.active.id - 1]) {
      this.tracks[this.active.id - 1].play();
    } else if (this.tracks[this._id]) {
      this.tracks[this._id].play();
    }
  }

  public add(track: Track): { id: number, remove: () => void } {
    const id = this.nextId();

    if (__DEV__) {
      console.log('[id]', id);
    }

    this.tracks[id] = track;
    track.id = id;

    return {
      id,
      remove: () => {
        if (this.active === track) {
          this.active.pause();
          this.active = null;
        }

        setTimeout(() => delete this.tracks[id]);
      }
    };
  }

  public playPause(): void {
    if (!this.active || this.active.paused) {
      this.play();
    } else {
      this.pause();
    }
  }

  public play(): void {
    if (!this.active && !this.hasTracks()) {
      return;
    }

    (this.active = (this.active || this.firstTrack())).play();

    if (__DEV__) {
      console.log(this.active);
    }

    this.volume = this.active.volume;
  }

  public pause(): void {
    this.apply(track => track.paused || track.pause());
  }

  public onEnded(id: number): void {
    this.active.load();

    const nextTrack = this.nextTrack(id);

    if (nextTrack) {
      (this.active = nextTrack).play();
    }
  }

  public onPlay(id: number): void {
    this.active = this.tracks[id];

    // Pause all others tracks
    this.apply((track: Track, i: number) => {
      if (i !== id && !track.paused) {
        track.pause();
      }
    });

    this.playing = true;
  }

  public onPause(id?: number): void {
    this.playing = !this.active.paused;
  }

  public apply(callback: ApplyFunc): void {
    Object.entries(this.tracks).forEach(([i, track]) => callback(track, Number(i)));
  }

  public mute(): void {
    this.apply(track => {
      track.mute();
    });

    this.muted = true;
  }

  public unmute(): void {
    this.apply(track => {
      track.unmute();
      track.volume = this.volume;
    });

    this.muted = false;
  }

  public volumeToggle(): void {
    if (this.muted) {
      this.unmute();
    } else {
      this.mute();
    }
  }

  public shuffle(): void {
    //
  }

  public repeat(): void {
    //
  }

  public onTimeUpdate(id: number): void {
    //
  }

  public hasTracks() {
    return Object.keys(this.tracks).length > 0;
  }

  public firstTrack(): Track {
    return Object.values(this.tracks)[0] || null;
  }

  public nextTrack(id: number): Track {
    return this.tracks[id + 1] || null;
  }

  private nextId() {
    return (
      this._id = this.sequence()
    );
  }

  private backward() {
    // this.apply(this.active)
  }

  private forward() {
    // this.apply(this.active)
  }
}

export const AudioPlayerComponent: IComponentOptions = {
  bindings: {
    sources: '<',
    onRemove: '&?'
  },
  template: require('./audio-player.html'),
  controller: AudioPlayer
};
