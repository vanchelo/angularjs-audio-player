import * as angular from 'angular';
import player from './Player';

import 'normalize.css/normalize.css';
import './styles.css';
import './player.css';

const rootElement = document.getElementById('root');

angular.bootstrap(rootElement, [player], {
  strictDi: true
});
