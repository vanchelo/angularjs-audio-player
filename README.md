# Audio Player

Audio Player powered by AngularJS and TypeScript.

## Requirements
- npm >= 6.10
- nodejs >= 10.15 

## Install
```
npm install
```

## Development
Run development server at http://localhost:8080
```
npm start
```

## Build
```
npm run build
```
