/**
 * @constructor
 */
export interface IAudioSource {
  paused: boolean;
  muted: boolean;
  volume: number;
  duration: number;
  currentTime: number;

  play(): any;
  pause(): void;
  load(): void;
  addEventListener(eventName: string, handler: (event: Event) => void, options?: any): void;
}
