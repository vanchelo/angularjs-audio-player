import * as template from './audio-track.html';
import { Track } from '../Track';
import { AudioSource } from '../AudioSource';
import { AudioPlayer } from '../AudioPlayer/audio-player.component';

import './audio-track.css';

interface IAudioSrc {
  url: string;
}

interface IScope extends ng.IScope {
  src: IAudioSrc;
  onRemove: () => void;
  track: Track;
  play: () => void;
  handleSliderClick: ($event: number) => void;
}

export const AudioTrack = (): ng.IDirective<IScope> => {
  'ngInject';

  return {
    restrict: 'E',
    require: '^^audioPlayer',
    template,
    scope: {
      src: '<',
      onRemove: '&?'
    },
    link(
      scope: IScope,
      element: ng.IAugmentedJQuery,
      attrs: ng.IAttributes,
      audioPlayer: AudioPlayer
    ) {
      const audio = createAudio(scope.src.url);
      const track = new Track(scope.src.url, audio);
      const { id, remove } = audioPlayer.add(track);

      const timeupdateHandler = () => {
        scope.$applyAsync(() => {
          // audioPlayer.onTimeUpdate(id);
        });
      };

      const endedHandler = () => {
        scope.$applyAsync(() => {
          audioPlayer.onEnded(id);
        });
      };

      const pauseHandler = () => {
        scope.$applyAsync(() => {
          audioPlayer.onPause(id);
        });
      };

      const playHandler = () => {
        scope.$applyAsync(() => {
          audioPlayer.onPlay(id);
        });
      };

      const removeListeners = () => {
        audio.removeEventListener('play', playHandler);
        audio.removeEventListener('pause', pauseHandler);
        audio.removeEventListener('ended', endedHandler);
        audio.removeEventListener('timeupdate', timeupdateHandler);
      };

      audio.onloadedmetadata = () => scope.$applyAsync();

      scope.track = track;

      scope.play = () => {
        if (track.paused) {
          track.play()
            .then(() => {
              console.log('playing');
            })
            .catch((error: any) => {
              track.pause();
              console.log('error', error);
            });
        } else {
          track.pause();
        }
      };

      scope.handleSliderClick = ($event: number) => {
        audio.currentTime = $event;
      };

      audio.addEventListener('play', playHandler);
      audio.addEventListener('pause', pauseHandler);
      audio.addEventListener('ended', endedHandler);
      audio.addEventListener('timeupdate', timeupdateHandler, { capture: false, passive: true });

      scope.$on('$destroy', () => {
        removeListeners();
        remove();
      });
    }
  };
};

function createAudio(src: string): AudioSource {
  const audio: AudioSource = document.createElement('audio');
  const source = document.createElement('source');

  source.src = src;
  source.type = 'audio/mpeg; codec=vorbis';
  audio.controls = true;
  audio.autoplay = false;
  audio.preload = 'metadata';
  audio.appendChild(source);

  return audio;
}
